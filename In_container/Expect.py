import os
import time
import sys
from pexpect import *
import pexpect
from pexpect import pxssh
import re

pwd = os.getenv('PASSWORD')
user_env = os.getenv('USER')
ip = os.getenv('HOST')
command = os.getenv('COMMAND')
connstr = 'ssh ' + str(user_env) + '@' + str(ip)



child = pxssh.pxssh()
#hostname = raw_input('hostname: ')
#username = raw_input('username: ')
#password = getpass.getpass('password: ')
child.login(ip, user_env, pwd)
time.sleep(2)
child.sendline(command)   # run a command
child.prompt()             # match the prompt
pp = child.before        # print everything before the prompt.
ansi_escape_8bit = re.compile(br'''(?: # either 7-bit C1, two bytes, ESC Fe (omitting CSI)
    \x1B
    [@-Z\\-_]
|   # or a single 8-bit byte Fe (omitting CSI)
    [\x80-\x9A\x9C-\x9F]
|   # or CSI + control codes
    (?: # 7-bit CSI, ESC [ 
        \x1B\[
    |   # 8-bit CSI, 9B
        \x9B
    )
    [0-?]*  # Parameter bytes
    [ -/]*  # Intermediate bytes
    [@-~]   # Final byte
)''', re.VERBOSE)
result = ansi_escape_8bit.sub(b'', pp)
print(result)
